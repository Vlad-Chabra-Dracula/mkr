<?php


use PHPUnit\Framework\TestCase;
use Kholodylnyk;

class KholodylnykTest extends TestCase
{
    public function testBestPrice()
    {
        $stub = $this
            ->getMockBuilder(Kholodylnyk::class)
            ->disableOriginalConstructor()
            ->getMock();

        $stub->method('getPrice')
            ->willReturn('5000');

        $this->assertSame('5000', $stub->getPrice());
    }

    public function testPrice($actual, $expected)
    {
        $result = $this->price = $actual;
        $this->assertEquals($expected, $result);
    }

    public function PriceDataProvider(): array
    {
        return array(
            array(1, 1, 2),
            array(0, 0, 0),
            array(-1, 0, -2),
        );
    }
}
