<?php

include 'Kholodylnyk.php';

class Shop
{
    private array $kholodylnyks;

    public function __construct()
    {
        $this->kholodylnyks = array(
            new Kholodylnyk('LG', 'small', 5000),
            new Kholodylnyk('Lego', 'average', 2500),
            new Kholodylnyk('Fig', 'average', 6000),
            new Kholodylnyk('Samsung', 'big', 10000),
        );
    }

    public function bestPriceKholodylnyk($price){
        foreach ($this->kholodylnyks as $kholodylnyk){
            if ($kholodylnyk->getPrice() == $price)
                return $kholodylnyk->getPrice();
        }
        return null;
    }
    public function getKholodylnyksByBrand($kholodylnyk_count){
        $kholodylnyks = array();

        foreach ($this->kholodylnyks as $kholodylnyk){
            if($kholodylnyk->getBrand() >= $kholodylnyk_count){
                $kholodylnyks[] = $kholodylnyk;
            }
        }
        return $kholodylnyks;
    }
}